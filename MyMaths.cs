﻿using System;
using System.Diagnostics.Contracts;

namespace ClassLibrary1
{
    public static class MyMaths
    {
        public static int DivideInts(int x, int y)
        {
            Contract.Requires(y != 0, "Divide by Zero");

            return x/y;
        }

        public static int Increment(int x)
        {
            Contract.Requires(x < int.MaxValue, "Overflow!");
            Contract.Ensures(Contract.Result<int>() == x + 1);

            // The static checker can determine that we can't oblige the postcondition...
            return x;
            // return x+1;
        }

        public static int PercentageAdd(int x, int y)
        {
            // The predicates can be quite convoluted:
            Contract.Requires(x + y <= 100);
            Contract.Requires(x <= 100);
            Contract.Requires(y <= 100);
            Contract.Ensures(Contract.Result<int>() <= 100);

            return x + y;
        }

        public static int ReturnsBigNumber()
        {
            Contract.Ensures(Contract.Result<int>() > 1000);
            return 12345;
        }

        public static int ReturnsSmallNumber()
        {
            Contract.Ensures(Contract.Result<int>() < 8);
            return 5;
        }

        public static void BigNumberNeeded(int x)
        {
            Contract.Requires(x > 1000);
            // ..
        }

        public static void SmallNumberNeeded(int x)
        {
            Contract.Requires(x < 10);
            // ..
        }


        /// <summary>
        /// There is no need to rewrite existing Guards
        /// </summary>
        /// <param name="x"></param>
        public static void LegacyCheck(int x)
        {
            if (x < 0)
                throw new ArgumentException(nameof(x));
            Contract.EndContractBlock();

            // ..
        }
    }
}