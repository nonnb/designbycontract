﻿namespace LegacyLibrary
{
    /// <summary>
    /// Sometimes we need to work with libraries which have no contracts. 
    /// In this case, in the CALLING code, we can use Contract.Assume to Assert the result
    /// This then adds the 'fact' to the static checker for further checks
    /// </summary>
    public class Legacy
    {
        public static int ReturnSmallNumber()
        {
            return 1;
        }

        public static int ReturnBigNumber()
        {
            return 12345;
        }
    }
}
