﻿using System;
using System.Diagnostics.Contracts;

namespace ClassLibrary1
{
    // For interfaces, we need to have a hacky 'companion' class for the interface which holds the contracts for the interface methods
    [ContractClass(typeof(InterfaceContracts))]
    public interface IMyInterface
    {
        double Divide(double number, double divisor);
    }

    // We can't add contracts to subclassed overridden (abstract or interface) methods
    // Look for https://stackoverflow.com/a/20861211/314291 Violation - Pre condition is strengthened by a subtype
    public class ImplementationA : IMyInterface
    {
        public double Divide(double number, double divisor)
        {
            // No! The client will be dependendant on the interface, not this concrete class, so cannot have a contract
            // here. Contract is on the interface
            Contract.Requires(Math.Abs(divisor) > 0.001);
            return number / divisor;
        }
    }

    public class ImplementationB : IMyInterface
    {
        public double Divide(double number, double divisor)
        {
            // No!
            Contract.Requires(Math.Abs(divisor) > 0.01);
            return number / divisor;
        }
    }

    [ContractClassFor(typeof(IMyInterface))]
    public class InterfaceContracts : IMyInterface
    {
        public double Divide(double number, double divisor)
        {
            // Only one contract is allowed for all implementations
            Contract.Requires(Math.Abs(divisor) > 0.01);
            throw new NotImplementedException();
        }
    }
}