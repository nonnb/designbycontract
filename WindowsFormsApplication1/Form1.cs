﻿using System;
using System.Diagnostics.Contracts;
using System.Windows.Forms;
using ClassLibrary1;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Comment out once done + rebuild.
            // MyMaths.DivideInts(5, 0);

            var random = new Random();
            if (random.Next() > 0)
                MyMaths.DivideInts(5, 0);

            var someNumber = random.Next(int.MaxValue);
            MyMaths.DivideInts(5, someNumber);
        }

        /// <summary>
        /// Static analysis is able to 'chain' together the postconditions in order to prove a precondition, e.g.
        /// </summary>
        public void ChainedBurdenOfProof()
        {
            var small = MyMaths.ReturnsSmallNumber();
            MyMaths.BigNumberNeeded(small);

            // Same for (need to comment above)
            var big = MyMaths.ReturnsBigNumber();
            MyMaths.SmallNumberNeeded(big);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var poco = new MyPoco
            {
                FirstName = "Foo",
                Surname = "Bar"
            };

            poco.Surname = null;

            // So, what's the problem?
            poco.GetFullName();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Hopefull, one day, all code will be like this ... immutable POCOs
            var poco = new MyPoco("Foo", "Bar");
            poco.GetFullName();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            // Run this with, and without release mode
            MyMaths.LegacyCheck(-1);
        }

        public void UseContractInterface(IMyInterface someInjectedMathsProvider)
        {
            // In reality, this would be bootstrapped from the IoC container, and we would set the container policy to 
            // crash the IoC if a dependency can't be resolved, rather than returning null.
            Contract.Requires(someInjectedMathsProvider != null, "No, do NOT pass me null!!!!");

            // So which contract applies here?
            var quotient = someInjectedMathsProvider.Divide(134, 0);

            // (This relates to one of the violations of the Liskov Substitution Principal - Contract cannot be strengthened by a subtype)
        }
    }
}
