﻿using System.Configuration;
using System.Diagnostics.Contracts;

namespace ClassLibrary1
{
    public class MyPoco
    {
        public MyPoco()
        {
        }

        public MyPoco(string firstName, string surname)
        {
            FirstName = firstName;
            Surname = surname;
        }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string GetFullName()
        {
            Contract.Ensures(Contract.Result<string>() != null);
            Contract.Ensures(!string.IsNullOrEmpty(Contract.Result<string>()));
            return $"{FirstName} {Surname}";
        }

        /// <summary>
        /// Invariants on Auto properties are rewritten INTO the Getters and Setters
        /// https://stackoverflow.com/q/5513942/314291
        /// </summary>
        [ContractInvariantMethod]
        private void Invariant()
        {
            Contract.Invariant(!string.IsNullOrEmpty(FirstName));
            Contract.Invariant(!string.IsNullOrEmpty(Surname));
        }
    }
}
